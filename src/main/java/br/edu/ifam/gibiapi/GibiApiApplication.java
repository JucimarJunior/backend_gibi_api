package br.edu.ifam.gibiapi;

import br.edu.ifam.gibiapi.controller.GibiController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = "br.edu.ifam.gibiapi.controller")
public class GibiApiApplication {
	private static final Logger logger = LoggerFactory.getLogger(GibiController.class);

	public static void main(String[] args) {
		SpringApplication.run(GibiApiApplication.class, args);
	}
}
