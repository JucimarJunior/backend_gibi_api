package br.edu.ifam.gibiapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.edu.ifam.gibiapi.repository.GibiRepository;

@RequestMapping("meugibi")
public class GibiController {
	private GibiRepository gibiRepository;
	
	GibiController(GibiRepository gibiRepository) {
		this.gibiRepository = gibiRepository;
	}
	
	@GetMapping
	public Object getAll() {
		return gibiRepository.findAll();
		
	}
	
	@GetMapping("{id}")
	public Object getById(@PathVariable Long id) {
		return gibiRepository.findById(id);

	}
}
